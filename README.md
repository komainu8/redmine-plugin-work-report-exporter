# redmine-plugin-work-report-exporter

Redmine plugin to export work reports based on time entries, mainly for a workflow of incident/time based support business

## Install

```console
$ cd /path/to/redmine/plugins
$ git clone https://gitlab.com/redmine-plugin-work-report-exporter/redmine-plugin-work-report-exporter.git work_report_exporter
$ cd ..
$ bundle install
$ bin/rails work_report_exporter:initial_custom_field:create RAILS_ENV=production RAILS_LOCALE=en
```

Please note that you need to specify `RAILS_LOCALE` with your main locale.
(Just "en" and "ja" are available for now.)

And then restart your Redmine.


## Uninstall

```console
$ cd /path/to/redmine
$ bin/rails work_report_exporter:initial_custom_field:destroy RAILS_ENV=production RAILS_LOCALE=en
```

And then restart your Redmine.


## Tests

```console
$ cd /path/to/redmine
$ plugins/work_report_exporter/dev/run-test.sh
```


## License

GPL 3 or later. See LICENSE for details.

