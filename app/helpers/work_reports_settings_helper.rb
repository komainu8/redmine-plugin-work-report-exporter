# redmine-plugin-work-report-exporter
# Copyright (C) 2021,2022 ClearCode Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WorkReportsSettingsHelper
  module_function
  def project_custom_string_field_names
    collect_project_custom_field_name_with_format("string")
  end

  def project_custom_list_field_names
    collect_project_custom_field_name_with_format("list")
  end

  def project_custom_date_field_names
    collect_project_custom_field_name_with_format("date")
  end

  def project_custom_float_field_names
    collect_project_custom_field_name_with_format("float")
  end

  def project_custom_int_field_names
    collect_project_custom_field_name_with_format("int")
  end

  def version_custom_string_field_names
    collect_version_custom_field_name_with_format("string")
  end

  def version_custom_list_field_names
    collect_version_custom_field_name_with_format("list")
  end

  def version_custom_date_field_names
    collect_version_custom_field_name_with_format("date")
  end

  def version_custom_float_field_names
    collect_version_custom_field_name_with_format("float")
  end

  def version_custom_int_field_names
    collect_version_custom_field_name_with_format("int")
  end

  def collect_project_custom_field_name_with_format(format)
    ProjectCustomField.where(field_format: format).pluck(:name)
  end
  private_class_method :collect_project_custom_field_name_with_format

  def collect_version_custom_field_name_with_format(format)
    VersionCustomField.where(field_format: format).pluck(:name)
  end
  private_class_method :collect_version_custom_field_name_with_format
end
