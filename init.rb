# redmine-plugin-work-report-exporter
# Copyright (C) 2021  ClearCode Inc.
# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

Redmine::Plugin.register :work_report_exporter do
  name "Work Report Exporter plugin"
  author "ClearCode Inc."
  description "This plugin provides ability to export work reports (Excel spreadsheets) based on time entries, mainly for a workflow of incident/time based support business"
  version "0.0.3"
  url "https://gitlab.com/redmine-plugin-work-report-exporter/redmine-plugin-work-report-exporter"
  author_url "https://www.clear-code.com/"

  permission :work_reports, { work_reports: [:index] }, :public => true

  directory __dir__

  settings partial: "settings/work_report_exporter",
           default: {}
end

unless Mime[:xlsx]
  Mime::Type.register("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                      :xlsx)
end

# Load
OtherFormatsLinksXlsx

WorkReportExporter::Settings
class << Setting
  prepend WorkReportExporter::SettingsObjectize
end
