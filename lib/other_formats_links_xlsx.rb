# redmine-plugin-work-report-exporter
# Copyright (C) 2021,2022 ClearCode Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module OtherFormatsLinksXlsx
  def other_formats_links(*args)
    super(*args) do |builder|
      yield builder if block_given?
      if controller_name == "timelog" and action_name == "index" and params[:project_id]
        concat(builder.link_to_with_query_parameters(
                 "xlsx",
                 {controller: "work_reports"},
                 {caption: l("work_report_exporter.label.xlsx_link")}))
      end
    end
  end
end

TimelogController.helper(OtherFormatsLinksXlsx)
