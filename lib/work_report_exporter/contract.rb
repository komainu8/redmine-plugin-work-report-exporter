# redmine-plugin-work-report-exporter
# Copyright (C) 2022  ClearCode Inc.
# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WorkReportExporter
  class Contract
    attr_reader :version, :time_entries

    def initialize(version, time_entries)
      @version = version
      @time_entries = time_entries.select do |entry|
        entry.issue and entry.issue.fixed_version == @version
      end
      @settings = Setting.plugin_work_report_exporter
    end

    def valid?
      not @version.effective_date.nil?
    end

    def contract_id
      @contract_id ||= version_field_value(:contract_id)
    end

    def contract_start_date
      @contract_start_date ||= version_field_value(:contract_start_date)
    end

    def contract_end_date
      @version.effective_date
    end

    def n_incidents
      @n_incidents ||= version_field_value(:n_incidents)
    end

    def hours_per_incident
      @hours_per_incident ||= version_field_value(:hours_per_incident)
    end

    def sheet_format
      @sheet_format ||= version_field_value(:sheet_format)
    end

    def sheet_name
      @sheet_name ||= version_field_value(:sheet_name)
    end

    def service_title
      @service_title ||= version_field_value(:service_title)
    end

    def service_type
      @service_type ||= version_field_value(:service_type)
    end

    def count_start_date
      @count_start_date ||= version_field_value(:count_start_date)
    end

    def count_style
      @count_style ||= version_field_value(:count_style)
    end

    def count_weight
      @count_weight ||= version_field_value(:count_weight)
    end

    def detail_count_style
      @detail_count_style ||= version_field_value(:detail_count_style)
    end

    def date_stamp
      DateFormatter.format(contract_end_date)
    end

    private
    def version_field_value(name)
      field_name = @settings.field_name(name)
      return @settings.default_value(name) if field_name.blank?

      format = Settings::VERSION_ITEMS[name][:field_format] || "string"
      field = VersionCustomField
                .where(name: field_name,
                       field_format: format)
                .first
      return @settings.default_value(name) if field.nil?

      value = @version.custom_field_value(field.id)
      value = @settings.cast(value, format)
      value = @settings.default_value(name) if value.blank?
      value
    end
  end
end
