# redmine-plugin-work-report-exporter
# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WorkReportExporter
  module SettingsObjectize
    def plugin_work_report_exporter
      Settings.new(super)
    end
  end

  class Settings
    def initialize(raw)
      @raw = raw || {}
    end

    PROJECT_ITEMS = {
      company_name: {},
      company_title: {},
      provider_name: {},
      comment_sanitizer: {},
    }
    VERSION_ITEMS = {
      contract_id: {},
      sheet_name: {},
      sheet_format: {
        default_value: "summarized",
        field_format: "list",
        possible_values: ["summarized", "detailed"],
      },
      service_title: {},
      service_type: {
        default_value: "support",
        field_format: "list",
        possible_values: ["support", "development"],
      },
      contract_start_date: {
        field_format: "date",
      },
      count_start_date: {
        field_format: "date",
      },
      count_style: {
        default_value: "incident",
        field_format: "list",
        possible_values: ["incident", "hour"],
      },
      detail_count_style: {
        default_value: "incident",
        field_format: "list",
        possible_values: ["incident", "hour"],
      },
      count_weight: {
        default_value: 1.0,
        field_format: "float",
        min_length: 0,
      },
      n_incidents: {
        default_value: 0,
        field_format: "int",
        min_length: 0,
      },
      hours_per_incident: {
        default_value: 1,
        field_format: "int",
        min_length: 1,
      },
    }
    all_items = PROJECT_ITEMS.merge(VERSION_ITEMS)
    all_items.each do |name, options|
      default_value_key = "#{name}_default_value"
      default_value = options[:default_value]
      field_format = options[:field_format] || "string"
      if field_format == "string"
        default_value_i18n_key = "work_report_exporter.default_value.#{name}"
        define_method(default_value_key) do
          @raw[default_value_key] ||
            default_value ||
            I18n.t(default_value_i18n_key)
        end
      else
        define_method(default_value_key) do
          cast(@raw[default_value_key], field_format) ||
            default_value
        end
      end

      define_method("#{default_value_key}=") do |value|
        @raw[default_value_key] = value
      end

      field_name_key = "#{name}_field_name"
      field_name_i18n_key = "work_report_exporter.custom_field_name.#{name}"
      define_method(field_name_key) do
        @raw[field_name_key] || I18n.t(field_name_i18n_key)
      end

      define_method("#{field_name_key}=") do |value|
        @raw[field_name_key] = value
      end
    end

    def default_value(name)
      __send__("#{name}_default_value")
    end

    def set_default_value(name, value)
      __send__("#{name}_default_value=", value)
    end

    def field_name(name)
      __send__("#{name}_field_name")
    end

    def set_field_name(name, value)
      __send__("#{name}_field_name=", value)
    end

    def to_h
      @raw
    end

    def cast(value, format)
      return nil if value.blank?

      case format
      when "int"
        return value if value.is_a?(Integer)
        Integer(value, 10)
      when "float"
        return value if value.is_a?(Float)
        Float(value)
      when "date"
        return value if value.is_a?(Date)
        Date.parse(value)
      else
        value
      end
    end
  end
end
