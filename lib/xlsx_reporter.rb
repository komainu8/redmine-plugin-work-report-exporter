# redmine-plugin-work-report-exporter
# Copyright (C) 2021  ClearCode Inc.
# Copyright (C) 2018-2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "axlsx"
require "date"
require "time"

module XlsxReporter
  module ExtendedModels
    refine TimeEntry do
      def activity_key
        if product_support?
          activity.name
        else
          format_comments
        end
      end

      def product_support?
        activity.name == "プロダクトサポート"
      end

      def format_comments
        comments.
          gsub(/\A\d+\/\d+ ?/, "").
          gsub(/\A(?:\#\d+#note-\d+,? ?)+/, "").
          gsub(/\A(?:\#\d+,? ?)+/, "").
          gsub(/（.+?）/, "")
      end

      def format_detail(weight, detail_count_style, extra_sanitizer)
        formatted_comments = format_comments.gsub(extra_sanitizer, "")
        sum = hours * weight
        sum = sum.ceil if detail_count_style == "incident"
        "#{spent_on} #{formatted_comments}(#{sum}h)"
      end
    end

    refine Issue do
      def n_used_hours(weight)
        time_entries.sum do |time_entry|
          if time_entry.product_support?
            0
          else
            time_entry.hours * weight
          end
        end
      end

      def n_used_incidents(weight, hours_per_incident)
        (n_used_hours(weight).to_f / hours_per_incident).ceil
      end

      def grouped_time_entries
        time_entries.sort_by(&:spent_on).group_by do |time_entry|
          time_entry.activity_key
        end
      end
    end
  end
  using ExtendedModels

  class Contract < Struct.new(:project_id,
                              :company_name,
                              :company_title,
                              :provider_name,
                              :comment_sanitizer,
                              :contract_id,
                              :sheet_format,
                              :sheet_name,
                              :service_title,
                              :service_type,
                              :start_date,
                              :count_start_date,
                              :end_date,
                              :n_incidents,
                              :hours_per_incident,
                              :version,
                              :count_style,
                              :count_weight,
                              :detail_count_style,
                              :time_entries,
                              :created_date,
                              :keyword_init => true)
    def hour_based_count?
      service_type == "development" or count_style == "hour"
    end

    def issues
      @issues ||= collect_issues
    end

    def count_weight
      super || 1
    end

    def hours_per_incident
      super || 1
    end

    def base_name
      date = Date.new
      "%s-%04d-%02d" % [project_id, date.year, date.month]
    end

    def target_issue?(issue)
      version and
        issue.fixed_version == version and
        issue.time_entries.any? do |time_entry|
          time_entries.include?(time_entry)
        end
    end

    def target_issues
      issues.find_all do |issue|
        target_issue?(issue)
      end
    end

    def n_issues
      target_issues.size
    end

    def n_used_hours
      sum_time_entry(0.0) do |time_entry|
        if time_entry.product_support?
          0.0
        else
          time_entry.hours * count_weight
        end
      end
    end

    def n_product_support_hours
      sum_time_entry(0.0) do |time_entry|
        if time_entry.product_support?
          time_entry.hours * count_weight
        else
          0.0
        end
      end
    end

    def n_total_used_hours
      target_issues.sum do |issue|
        issue.n_used_hours(count_weight)
      end
    end

    def n_rest_hours
      n_incidents - n_total_used_hours
    end

    def n_used_incidents
      n_used_hours.ceil
    end

    def n_total_used_incidents
      target_issues.sum do |issue|
        issue.n_used_incidents(count_weight, hours_per_incident)
      end
    end

    def n_rest_incidents
      n_incidents - n_total_used_incidents
    end

    def company_name_with_title
      "#{company_name}　#{company_title}"
    end

    def comment_sanitizer_matcher
      @comment_sanitizer_matcher ||= Regexp.new(comment_sanitizer)
    end

    private
    def collect_issues
      issues = time_entries.collect do |time_entry|
        issue = time_entry.issue
      end
      issues.compact.uniq
    end

    def sum_time_entry(initial_value)
      sum = initial_value
      target_issues.each do |issue|
        issue.time_entries.each do |time_entry|
          next unless time_entries.include?(time_entry)
          sum += yield(time_entry)
        end
      end
      sum
    end
  end

  class SheetFiller
    def initialize(sheet, contract, styles)
      @sheet = sheet
      @contract = contract
      @styles = styles
    end

    def fill
      case @contract.sheet_format
      when "summarized" then
        fill_summarized
      else
        fill_detailed
      end
    end

    private
    def fill_detailed
      n_used_incidents_cell = fill_header_detailed

      fill_body_detailed

      widths = [1, 12]
      widths.concat([17, nil, 15, 10, 10, 25, 40])
      @sheet.column_widths(*widths)

      column = "G"
      n_used_incidents_cell.escape_formulas = false
      n_used_incidents_cell.value = "=SUM(#{column}11:#{column}#{@sheet.rows.size})"
    end

    def fill_header_detailed
      row = @sheet.add_row([nil])
      row.add_cell(@contract.service_title,
                   style: @styles[:title])
      @sheet.merge_cells("B1:J1")

      row = @sheet.add_row([nil])
      row.add_cell(@contract.company_name_with_title, style: @styles[:to])
      @sheet.merge_cells("B2:D2")

      row = @sheet.add_row([nil] * ("I".ord - "A".ord))
      row.add_cell(@contract.provider_name, style: @styles[:from])

      @sheet.add_row([], height: 3)

      row = @sheet.add_row([nil] * ("H".ord - "A".ord))
      row.add_cell("更新日", style: @styles[:item_title])
      row.add_cell(@contract.created_date.to_s,
                   style: @styles[:item_title])

      @sheet.add_row

      row = @sheet.add_row([nil] * ("C".ord - "A".ord))
      row.add_cell("契約期間", style: @styles[:item_title])
      row.add_cell("#{@contract.start_date}から#{@contract.end_date}",
                   style: @styles[:item_title])
      row.add_cell
      row.add_cell
      if @contract.hour_based_count?
        row.add_cell("実績工数", style: @styles[:item_title])
      else
        row.add_cell("消費インシデント数", style: @styles[:item_title])
      end
      row.add_cell("", style: @styles[:item_title])
      if @contract.hour_based_count?
        row.add_cell("残り工数", style: @styles[:item_title])
      else
        row.add_cell("残りインシデント数", style: @styles[:item_title])
      end
      @sheet.merge_cells("G7:H7")

      row = @sheet.add_row([nil] * ("C".ord - "A".ord))
      if @contract.service_type == "support"
        if @contract.hour_based_count?
          row.add_cell("契約工数", style: @styles[:item_title])
        else
          row.add_cell("契約インシデント", style: @styles[:item_title])
        end
      else
        row.add_cell("見積工数", style: @styles[:item_title])
      end
      row.add_cell(@contract.n_incidents,
                   style: @styles[:item_title])
      row.add_cell
      row.add_cell
      n_used_incidents_cell = row.add_cell(nil, style: @styles[:item_title])
      row.add_cell("", style: @styles[:item_title])
      row.add_cell("=D8-G8",
                   escape_formulas: false,
                   style: @styles[:item_title])
      @sheet.merge_cells("G8:H8")

      row = @sheet.add_row([nil])
      row.add_cell("チケット№", style: @styles[:item_title])
      row.add_cell("発生日", style: @styles[:item_title])
      row.add_cell("内容", style: @styles[:item_title])
      row.add_cell("対応完了日", style: @styles[:item_title])
      row.add_cell("実施内容", style: @styles[:item_title])
      row.add_cell("対応時間", style: @styles[:item_title])
      row.add_cell("", style: @styles[:item_title])
      row.add_cell("備考", style: @styles[:item_title])
      @sheet.merge_cells("G9:H9")

      row = @sheet.add_row([nil])
      ("G".ord - "B".ord).times do
        row.add_cell("", style: @styles[:item_title])
      end
      row.add_cell("計", style: @styles[:item_title])
      row.add_cell("内訳", style: @styles[:item_title])
      row.add_cell("", style: @styles[:item_title])

      ("B".."F").each do |column|
        @sheet.merge_cells("#{column}9:#{column}10")
      end
      @sheet.merge_cells("I9:I10")

      n_used_incidents_cell
    end

    def fill_body_detailed
      sorted_issues = @contract.target_issues.sort_by(&:id)
      sorted_issues.each do |issue|
        row = @sheet.add_row([nil])
        row.add_cell(issue.id, style: @styles[:item])
        row.add_cell(issue.start_date.to_s, style: @styles[:item])
        row.add_cell(issue.subject, style: @styles[:item_text])
        if issue.closed_on && issue.due_date
          due_date = issue.due_date.to_s
        else
          due_date = nil
        end
        row.add_cell(due_date, style: @styles[:item])
        grouped_time_entries = issue.grouped_time_entries
        activities = grouped_time_entries.keys.uniq
        row.add_cell(activities.join("\n"), style: @styles[:item_text])
        if @contract.hour_based_count?
          row.add_cell(issue.n_used_hours(@contract.count_weight),
                       style: @styles[:item_number])
        else
          row.add_cell(issue.n_used_incidents(@contract.count_weight,
                                              @contract.hours_per_incident),
                       style: @styles[:item_number])
        end
        statistics = grouped_time_entries.collect do |name, time_entries|
          format_summary(name, time_entries)
        end
        row.add_cell(statistics.join("\n"), style: @styles[:item_text])
        details = format_details(issue.time_entries)
        row.add_cell(details.join("\n"), style: @styles[:item_text])
        set_entry_row_size(row, details.size)
      end
    end

    def fill_summarized
      n_used_incidents_cell = fill_header_summarized

      fill_body_summarized

      widths = [1, 10, 17, 55, 22, 19, 54]
      @sheet.column_widths(*widths)

      column = "F"
      n_used_incidents_cell.escape_formulas = false
      n_used_incidents_cell.value = "=SUM(#{column}11:#{column}#{@sheet.rows.size})"
    end

    def fill_header_summarized
      row = @sheet.add_row([nil])
      row.add_cell(@contract.service_title,
                   style: @styles[:title])
      @sheet.merge_cells("B1:G1")

      row = @sheet.add_row([nil])
      row.add_cell(@contract.company_name_with_title, style: @styles[:to])
      @sheet.merge_cells("B2:D2")

      row = @sheet.add_row([nil] * ("G".ord - "A".ord))
      row.add_cell(@contract.provider_name, style: @styles[:from])

      @sheet.add_row([], height: 3)
      @sheet.add_row

      row = @sheet.add_row([nil] * ("C".ord - "A".ord))
      row.add_cell("契約番号", style: @styles[:item_title])
      row.add_cell(@contract.contract_id,
                   style: @styles[:item_title])
      row.add_cell
      row.add_cell("更新日", style: @styles[:item_title])
      row.add_cell(@contract.created_date.to_s,
                   style: @styles[:item_title])

      row = @sheet.add_row([nil] * ("C".ord - "A".ord))
      row.add_cell("契約期間", style: @styles[:item_title])
      row.add_cell("#{@contract.start_date}から#{@contract.end_date}",
                   style: @styles[:item_title])
      row.add_cell
      if @contract.hour_based_count?
        row.add_cell("実績工数", style: @styles[:item_title])
      else
        row.add_cell("消費インシデント数", style: @styles[:item_title])
      end
      n_used_incidents_cell = row.add_cell(nil, style: @styles[:item_title])

      row = @sheet.add_row([nil] * ("C".ord - "A".ord))
      if @contract.service_type == "support"
        if @contract.hour_based_count?
          row.add_cell("契約工数", style: @styles[:item_title])
        else
          row.add_cell("契約インシデント", style: @styles[:item_title])
        end
      else
        row.add_cell("見積工数", style: @styles[:item_title])
      end
      row.add_cell(@contract.n_incidents,
                   style: @styles[:item_title])
      row.add_cell
      if @contract.hour_based_count?
        row.add_cell("残り工数", style: @styles[:item_title])
      else
        row.add_cell("残りインシデント数", style: @styles[:item_title])
      end
      row.add_cell("=D8-G7",
                   escape_formulas: false,
                   style: @styles[:item_title])

      @sheet.add_row

      row = @sheet.add_row([nil])
      row.add_cell("チケット№", style: @styles[:item_title])
      row.add_cell("発生日", style: @styles[:item_title])
      row.add_cell("内容", style: @styles[:item_title])
      row.add_cell("対応完了日", style: @styles[:item_title])
      if @contract.hour_based_count?
        row.add_cell("対応時間", style: @styles[:item_title])
      else
        row.add_cell("消費インシデント数", style: @styles[:item_title])
      end
      row.add_cell("備考", style: @styles[:item_title])

      n_used_incidents_cell
    end

    def fill_body_summarized
      sorted_issues = @contract.target_issues.sort_by(&:id)
      sorted_issues.each do |issue|
        row = @sheet.add_row([nil])
        row.add_cell(issue.id, style: @styles[:item])
        row.add_cell(issue.start_date.to_s, style: @styles[:item])
        row.add_cell(issue.subject, style: @styles[:item_text])
        if issue.due_date
          due_date = issue.due_date.to_s
        else
          due_date = nil
        end
        row.add_cell(due_date, style: @styles[:item])
        if @contract.hour_based_count?
          row.add_cell(issue.n_used_hours(@contract.count_weight),
                       style: @styles[:item_number])
        else
          row.add_cell(issue.n_used_incidents(@contract.count_weight,
                                              @contract.hours_per_incident),
                       style: @styles[:item_number])
        end
        details = format_details(issue.time_entries)
        row.add_cell(details.join("\n"), style: @styles[:item_text])
        set_entry_row_size(row, details.size)
      end
    end

    def format_summary(name, time_entries)
      sum = time_entries.sum(&:hours) * @contract.count_weight
      sum = sum.ceil if @contract.detail_count_style == "incident"
      "・#{name}(#{sum}h)"
    end

    def format_details(time_entries)
      time_entries.sort_by(&:spent_on).collect do |time_entry|
        "・#{time_entry.format_detail(@contract.count_weight, @contract.detail_count_style, @contract.comment_sanitizer_matcher)}"
      end
    end

    def set_entry_row_size(row, n_details)
      if n_details == 1
        row.height = 14 * 3
      else
        row.height = 14 * n_details
      end
    end
  end

  class Reporter
    def initialize(params)
      customer = params[:customer]
      @contracts = params[:contracts].collect do |contract|
        Contract.new({
          project_id: params[:project_id],
          company_name: customer.company_name,
          company_title: customer.company_title,
          provider_name: customer.provider_name,
          comment_sanitizer: customer.comment_sanitizer,
          contract_id: contract.contract_id,
          sheet_format: contract.sheet_format,
          sheet_name: contract.sheet_name,
          service_title: contract.service_title,
          service_type: contract.service_type,
          start_date: contract.contract_start_date,
          count_start_date: contract.count_start_date,
          end_date: contract.contract_end_date,
          n_incidents: contract.n_incidents,
          hours_per_incident: contract.hours_per_incident,
          version: contract.version,
          count_style: contract.count_style,
          count_weight: contract.count_weight,
          detail_count_style: contract.detail_count_style,
          time_entries: contract.time_entries,
          created_date: (params[:created_date] || User.current.today),
        })
      end
    end

    def report
      @package = Axlsx::Package.new
      styles = create_styles
      @contracts.each do |contract|
        @package.workbook.add_worksheet(:name => "#{contract.contract_id} #{contract.sheet_name}") do |sheet|
          SheetFiller.new(sheet, contract, styles).fill
        end
      end
      @package
    end

    private
    def create_styles
      styles = @package.workbook.styles
      {
        title: styles.add_style(b: true,
                                alignment: {
                                  horizontal: :center
                                }),
        to: styles.add_style(u: true),
        from: styles.add_style(alignment: {
                                 horizontal: :right,
                               }),
        item_title: styles.add_style(border: {
                                       color: "00000000",
                                       style: :thin,
                                     },
                                     alignment: {
                                       horizontal: :center,
                                     }),
        item: styles.add_style(border: {
                                 color: "00000000",
                                 style: :thin,
                               },
                               alignment: {
                                 horizontal: :center,
                                 vertical: :top,
                               }),
        item_text: styles.add_style(border: {
                                      color: "00000000",
                                      style: :thin,
                                    },
                                    alignment: {
                                      vertical: :top,
                                      wrap_text: true,
                                    }),
        item_number: styles.add_style(border: {
                                        color: "00000000",
                                        style: :thin,
                                      },
                                      alignment: {
                                        vertical: :top,
                                      }),
      }
    end
  end
end
