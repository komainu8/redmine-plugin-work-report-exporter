# redmine-plugin-work-report-exporter
# Copyright (C) 2021  ClearCode Inc.
# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require File.expand_path("../../../test_helper", __FILE__)

module WorkReportExporter
  class WorkReportsControllerTest < Redmine::ControllerTest
    tests WorkReportsController

    fixtures :custom_fields
    fixtures :custom_fields_projects
    fixtures :custom_fields_trackers
    fixtures :custom_values
    fixtures :enabled_modules
    fixtures :enumerations
    fixtures :issue_categories
    fixtures :issue_statuses
    fixtures :issues
    fixtures :member_roles
    fixtures :members
    fixtures :projects
    fixtures :projects_trackers
    fixtures :roles
    fixtures :time_entries
    fixtures :trackers
    fixtures :users
    fixtures :versions

    include Redmine::I18n

    def setup
      settings = Setting.plugin_work_report_exporter
      settings.contract_id_field_name = "contract_id"
      Setting.plugin_work_report_exporter = settings.to_h
      field = VersionCustomField.create!({
        name: "contract_id",
        field_format: "string",
      })

      version.due_date = today
      version.custom_field_values
      version.custom_field_values = { field.id => "c1" }
      version.save!

      another_version.due_date = today
      another_version.status = "open"
      another_version.custom_field_values
      another_version.custom_field_values = { field.id => "c2" }
      another_version.save!

      issues = project.issues.select do |issue|
        issue.spent_hours > 0
      end

      issues.each do |issue|
        issue.fixed_version = version
        issue.save!
      end
      last_issue = issues.last
      last_issue.fixed_version = another_version
      last_issue.save!
    end

    def test_direct_issue_fixed_version_id
      @request.session[:user_id] = 2

      get :index, :params => {
            project_id: project.identifier,
            "issue.fixed_version_id": version.id,
            format: "xlsx",
          }
      assert_response :success

      text = <<-TEXT

Work Report of the Service
Unknown Company	　
Unknown Provider


契約番号	c1	更新日	#{today}
契約期間	から#{today}	消費インシデント数
契約インシデント	0	残りインシデント数

チケット№	発生日	内容	対応完了日	消費インシデント数	備考
1	#{1.day.ago.to_date.to_s(:db)}	Cannot print recipes	#{10.day.from_now.to_date.to_s(:db)}	155	・2007-03-12 (150h)
・2007-03-23 My hours(5h)
      TEXT
      assert_equal([
                     "#{xlsx_mime_type}; header=present",
                     "Unknown Company-#{today_date_stamp}.xlsx",
                     text,
                   ],
                   [
                     response.content_type,
                     response.content_disposition_filename,
                     response.extract_text,
                   ])
    end

    def test_multiple_versions
      @request.session[:user_id] = 2

      get :index, :params => {
            project_id: project.identifier,
            format: "xlsx",
          }
      assert_response :success

      text = <<-TEXT

Work Report of the Service
Unknown Company	　
Unknown Provider


契約番号	c1	更新日	#{today}
契約期間	から#{today}	消費インシデント数
契約インシデント	0	残りインシデント数

チケット№	発生日	内容	対応完了日	消費インシデント数	備考
1	#{1.day.ago.to_date.to_s(:db)}	Cannot print recipes	#{10.day.from_now.to_date.to_s(:db)}	155	・2007-03-12 (150h)
・2007-03-23 My hours(5h)

Work Report of the Service
Unknown Company\t　
Unknown Provider


契約番号\tc2\t更新日\t#{today}
契約期間\tから#{today}\t消費インシデント数
契約インシデント\t0\t残りインシデント数

チケット№\t発生日\t内容\t対応完了日\t消費インシデント数\t備考
3	#{15.day.ago.to_date.to_s(:db)}	Error 281 when updating a recipe	#{5.day.ago.to_date.to_s(:db)}	1	・2007-04-21 (1h)
      TEXT
      assert_equal([
                     "#{xlsx_mime_type}; header=present",
                     "Unknown Company-#{today_date_stamp}.xlsx",
                     text,
                   ],
                   [
                     response.content_type,
                     response.content_disposition_filename,
                     response.extract_text,
                   ])
    end

    def test_ignore_expired_version
      @request.session[:user_id] = 2

      another_version.due_date = yesterday
      another_version.save!

      get :index, :params => {
            project_id: project.identifier,
            format: "xlsx",
          }
      assert_response :success

      text = <<-TEXT

Work Report of the Service
Unknown Company	　
Unknown Provider


契約番号	c1	更新日	#{today}
契約期間	から#{today}	消費インシデント数
契約インシデント	0	残りインシデント数

チケット№	発生日	内容	対応完了日	消費インシデント数	備考
1	#{1.day.ago.to_date.to_s(:db)}	Cannot print recipes	#{10.day.from_now.to_date.to_s(:db)}	155	・2007-03-12 (150h)
・2007-03-23 My hours(5h)
      TEXT
      assert_equal([
                     "#{xlsx_mime_type}; header=present",
                     "Unknown Company-#{today_date_stamp}.xlsx",
                     text,
                   ],
                   [
                     response.content_type,
                     response.content_disposition_filename,
                     response.extract_text,
                   ])
    end

    private
    def project
      @project ||= Project.find("ecookbook")
    end

    def version
      @version ||= Version.find_by!(project: project,
                                    name: "2.0")
    end

    def another_version
      @another_version ||= Version.find_by!(project: project,
                                            name: "1.0")
    end

    def today
      User.current.today.to_s(:db)
    end

    def yesterday
      User.current.today.yesterday.to_s(:db)
    end

    def today_date_stamp
      User.current.today.strftime("%Y-%m")
    end

    def xlsx_mime_type
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    end
  end
end
