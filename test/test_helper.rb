# redmine-plugin-work-report-exporter
# Copyright (C) 2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require File.expand_path("../../../../test/test_helper", __FILE__)

ChupaText::Decomposers.load

module ResponseTextExtractable
  def content_disposition_filename
    disposition = headers["Content-Disposition"]
    return nil if disposition.blank?
    disposition.split(/\s*;\s*/)[1..-1].each do |directive|
      key, value = directive.split("=", 2)
      case key
      when "filename", "filename*"
        return value[1..-2] # Remove double quotes
      end
    end
    nil
  end

  def extract_text
    filename = content_disposition_filename
    return nil if filename.nil?

    extractor = ChupaText::Extractor.new
    configuration = ChupaText::Configuration.default.dup
    configuration.decomposer.names = [
      "office-open-xml-workbook",
    ]
    extractor.apply_configuration(configuration)
    uri = Pathname(filename)
    input = StringIO.new(body)
    data = ChupaText::VirtualFileData.new(uri, input)
    bodies = []
    extractor.extract(data) do |extracted|
      bodies << extracted.body
    end
    bodies.join("\n")
  end
end

ActionDispatch::TestResponse.include(ResponseTextExtractable)
